# CCDetector

# Description
A machine learning based detector of Command and Control channels in malware and botnet traffic. See please www.researchgate.net/profile/Sebastian_Garcia6
It uses an implementation of Markov Chains to model the state transitions of the traffic according to a model and detect similar behavioral traffic in other binetflow files.

It can also read binetflow files in real time from the network and print a nice ncurses interface with the states.

## Usages
### Training 
- You should give it a __labeled__ netflow text file with -f (generated by Argus) and use -r. See the example file for details.
This will generate a MCModels folder full of the markov chain models for the tuples in the file.

### Testing
After training the models with some file. Use -f to give a __labeled__ binetflow file and use -e. 
Without any other option, a new file will be generated with the original binetflow information and an _additional_ column with the predicted label based on the trained models. Also a sorted version of the binetflow file is created.
No input is printed in the console.

## Verification and performance metrics
To verify the results and know the performance metrics you should use another program called BotnetDetectorsComparer (https://bitbucket.org/eldraco2000/botnetdetectorscomparer)
With this program you do:

    BotnetDetectorsComparer.py -f <binetflowfile>.labeled.sorted -t weight -T 300 -a 0.01

And find out the performance metrics according to a time window and weighted logic. Please see the papers.


# Versions
- 0.90
    This is the first public version. Any problem please contact sebastian.garcia@agents.fel.cvut.cz or eldraco@gmail.com


# Options
    usage: ./CCDetector.py <options>
    options:
      -h, --help                 Show this help message and exit
      -V, --version              Output version information and exit
      -D, --debug                Debug level. E.g -D 3 .
      -f, --file                 Input netflow file to analize. If - is used, netflows are read from stdin. Remember to pass the header!
      -t, --time-threshold       First Threshold of time difference.
      -b, --bytes-threshold      First Threshold of bytes size.
      -d, --duration-threshold   First Threshold of duration.
      -T, --analyze-tuple        Analyze only this tuple and print detailed information for each netflow.
      -u, --tuple-mode           The tuple mode. can be 3 for sip-dip-dport or 4 for sip-sport-dip-dport
      -R, --thresholds           Threshold mode. Prints all the values of the features for training the thresholds.
      -r, --training             Training mode. Read one binetflow from -f file and outputs one Markov Chain for each label in the folder 'MCModels'. Don't use -r, -v or -e at the same time.
      -v, --validation           Validation mode. Read one or several binetflow files (comma separated in -f ) and consider them as training-validation. Use 10-folds to compute the models, applies the models and get the best thresholds. Don't use -r, -v or -e at the same time.
      -e, --testing              Testing mode. Read binetflows from -f file, Markov Chains from the folder 'MCModels', predicts for each tuple the chain (label) with more probability of generating it and it outputs a labeled netflow file. Don't use -r, -v or -e at the same time.
      -w, --without-colors       Do not use colors in the output
      -l, --state-length         Minimun length to consider the state string for analysis
      -p, --min-prob-threshold   Threshold to use when comparing each tuple to every model. If -q is specified then this is the minimal threshold to try. You can also specify it as 1e-10. The lower limit is 1e-45.
      -q, --max-prob-threshold   Maximum threshold to use when comparing each tuple to every model. -p must be specified also. You can also specify it as 1e-11. The lower limit is 1e-45.
      -L, --label                Print all the informatin about all the tuples with this label.
      -P, --print-mode           Print mode: normal, csv, oneline, and epoch. You can combine them with '-'.
      -a, --all-models           Generate and include all models in the process. By default it only uses the models of the C&Cs. Only for the training.
      -n, --num-folds            Number of folds in the k-fold validation.
      -s, --step-threshold       Step to use when moving the threshold. Defaults to 10. That is from 0.1 to 0.01. Use multiples of 10.

