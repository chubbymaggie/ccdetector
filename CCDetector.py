#! /usr/bin/env python
#  Copyright (C) 2009  Sebastian Garcia
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Author:
# Sebastian Garcia, sebastian.garcia@agents.fel.cvut.cz, sgarcia@exa.unicen.edu.ar, eldraco@gmail.com
#

# Description
# A tool to detect botnet Command and Control channels using a behavioral model.


# standard imports
import getopt
import sys
import re
import os
import numpy as np
from datetime import datetime
from datetime import timedelta
import cPickle
import operator
import time
import pykov
import math
import subprocess
from sklearn.cross_validation import KFold
####################
# Global Variables

debug = 0
vernum = "0.90"
without_colors = False
model_id = 0
#########


# Print version information and exit
def version():
    print "+----------------------------------------------------------------------+"
    print "| CCDetector.py Version "+ vernum +"                                           |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print


# Print help information and exit:
def usage():
    print "+----------------------------------------------------------------------+"
    print "| CCDetector.py Version "+ vernum +"                                           |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print "\nusage: %s <options>" % sys.argv[0]
    print "options:"
    print "  -h, --help                 Show this help message and exit"
    print "  -V, --version              Output version information and exit"
    print "  -D, --debug                Debug level. E.g -D 3 ."
    print "  -f, --file                 Input netflow file to analize. If - is used, netflows are read from stdin. Remember to pass the header!"
    print "  -t, --time-threshold       First Threshold of time difference."
    print "  -b, --bytes-threshold      First Threshold of bytes size."
    print "  -d, --duration-threshold   First Threshold of duration."
    print "  -T, --analyze-tuple        Analyze only this tuple and print detailed information for each netflow."
    print "  -u, --tuple-mode           The tuple mode. can be 3 for sip-dip-dport or 4 for sip-sport-dip-dport"
    print "  -R, --thresholds           Threshold mode. Prints all the values of the features for training the thresholds."
    print "  -r, --training             Training mode. Read one binetflow from -f file and outputs one Markov Chain for each label in the folder 'MCModels'. Don't use -r, -v or -e at the same time."
    print "  -v, --validation           Validation mode. Read one or several binetflow files (comma separated in -f ) and consider them as training-validation. Use 10-folds to compute the models, applies the models and get the best thresholds. Don't use -r, -v or -e at the same time."
    print "  -e, --testing              Testing mode. Read binetflows from -f file, Markov Chains from the folder 'MCModels', predicts for each tuple the chain (label) with more probability of generating it and it outputs a labeled netflow file. Don't use -r, -v or -e at the same time."
    print "  -w, --without-colors       Do not use colors in the output"
    print "  -l, --state-length         Minimun length to consider the state string for analysis"
    print "  -p, --min-prob-threshold   Threshold to use when comparing each tuple to every model. If -q is specified then this is the minimal threshold to try. You can also specify it as 1e-10. The lower limit is 1e-45."
    print "  -q, --max-prob-threshold   Maximum threshold to use when comparing each tuple to every model. -p must be specified also. You can also specify it as 1e-11. The lower limit is 1e-45."
    print "  -L, --label                Print all the informatin about all the tuples with this label."
    print "  -P, --print-mode           Print mode: normal, csv, oneline, and epoch. You can combine them with '-'."
    print "  -a, --all-models           Generate and include all models in the process. By default it only uses the models of the C&Cs. Only for the training."
    print "  -n, --num-folds            Number of folds in the k-fold validation."
    print "  -s, --step-threshold       Step to use when moving the threshold. Defaults to 10. That is from 0.1 to 0.01. Use multiples of 10."
    print
    sys.exit(1)


class histograms:
    """
    Class to manage the histograms for each letter on each label
    """
    def __init__(self):
        # {'label1': { 'ftv':[], 'stv':[], ... , 'sth':[], 'tth':[], 'fth':[], 'fdh':[], 'sdh':[], 'tdh':[], 'fsh':[], 'ssh':[], 'tsh':[] }
        # the ftv, stv, etc, hold all the values!, the sth, etc hold the histograms, the stb, etc hold the bins
        self.data = {}
        self.stb = []
        self.ttb = []
        self.ftb = []
        self.fdb = []
        self.sdb = []
        self.tdb = []
        self.fsb = []
        self.ssb = []
        self.tsb = []
        self.rb = []

    def add_point(self, label, letter, TD, duration, size, srcdstbytesratio):
        """
        Get the data and store it for later processing. Store all the values for all the typs for all the labels.
        """
        global debug
        try:
            try:
                label_dict = self.data[label]
                # is there
            except KeyError:
                label_dict = {}

            # Time
            if letter in '123456789':
                # First time histogram
                pass
            elif letter in 'abcdefghi':
                # Second time histogram
                try:
                    # get the vector of values
                    stv = label_dict['stv']
                    # we have it
                except KeyError:
                    stv = [] 
                # Store all the values in the vector of values.
                stv.append(TD)
                label_dict['stv'] = stv

            elif letter in 'ABCDEFGHI':
                # Third time histogram
                try:
                    ttv = label_dict['ttv']
                    # we have it
                except KeyError:
                    ttv = []
                # Store all the values in the vector of values.
                ttv.append(TD)
                label_dict['ttv'] = ttv

            elif letter in 'rstuvwxyz':
                # Fourth time histogram
                try:
                    ftv = label_dict['ftv']
                    # we have it
                except KeyError:
                    ftv = []
                ftv.append(TD)
                label_dict['ftv'] = ftv


            # Duration. Warning! The values can not be lower than the first bin!
            if letter in '147adgADGrux': # First duration histogram
                try:
                    fdv = label_dict['fdv']
                    # we have it
                except KeyError:
                    fdv = []
                fdv.append(duration)
                label_dict['fdv'] = fdv

            elif letter in '258behBEHsvy':
                # Second duration histogram
                try:
                    sdv = label_dict['sdv']
                    # we have it
                except KeyError:
                    sdv = []
                sdv.append(duration)
                label_dict['sdv'] = sdv

            elif letter in '369cfiCFItwz':
                # Third duration histogram
                try:
                    tdv = label_dict['tdv']
                    # we have it
                except KeyError:
                    tdv = []
                tdv.append(duration)
                label_dict['tdv'] = tdv

            # Size
            if letter in '123abcABCrst':
                # First size histogram
                try:
                    fsv = label_dict['fsv']
                    # we have it
                except KeyError:
                    fsv = []
                fsv.append(size)
                label_dict['fsv'] = fsv

            elif letter in '456defDEFuvw':
                # Second size histogram
                try:
                    ssv = label_dict['ssv']
                    # we have it
                except KeyError:
                    ssv = []
                ssv.append(size)
                label_dict['ssv'] = ssv 

            elif letter in '789ghiGHIxyz':
                # Third size histogram
                try:
                    tsv = label_dict['tsv']
                    # we have it
                except KeyError:
                    tsv = []
                tsv.append(size)
                label_dict['tsv'] = tsv


            # First ratio histogram
            try:
                rv = label_dict['rv']
                # we have it
            except KeyError:
                rv = []
            rv.append(srcdstbytesratio)
            label_dict['rv'] = rv


            self.data[label] = label_dict
        except Exception as inst:
            print 'Problem in add_point() in class histograms'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)



    def compute_histograms(self):
        """
        Compute the histogram
        """
        try:
            for label in self.data.keys():
                label_dict = self.data[label]
                #print 'Label: {}'.format(label)
                # fth (first no)

                # sth
                try:
                    values = self.data[label]['stv']

                    n = len(values)
                    # Integer division
                    nbins = np.ceil(3.5 * np.std(values) // np.power(n, 1/3)) # Scott's normal reference rule, https://en.wikipedia.org/wiki/Histogram#Number_of_bins_and_width
                    if nbins == 0: # The previous formula can give 0
                        nbins = 1
                    # Compute the histogram
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['sth'] = histogram / float(n)
                    self.data[label]['stb'] = bins
                    #print '\tSTH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['sth']
                except KeyError:
                    pass # This label does not have stv

                # tth
                try:
                    values = self.data[label]['ttv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['tth'] = histogram / float(n)
                    self.data[label]['ttb'] = bins
                    #print '\tTTH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['tth']
                except KeyError:
                    pass # This label does not have stv
                # fth
                try:
                    values = self.data[label]['ftv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['fth'] = histogram / float(n)
                    self.data[label]['ftb'] = bins
                    #print '\tFTH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['fth']
                except KeyError:
                    pass # This label does not have stv
                # fdh
                try:
                    values = self.data[label]['fdv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['fdh'] = histogram / float(n)
                    self.data[label]['fdb'] = bins
                    #print '\tFDH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['fdh']
                except KeyError:
                    pass # This label does not have stv
                # sdh
                try:
                    values = self.data[label]['sdv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['sdh'] = histogram / float(n)
                    self.data[label]['sdb'] = bins
                    #print '\tSDH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['sdh']
                except KeyError:
                    pass # This label does not have stv
                # tdh
                try:
                    values = self.data[label]['tdv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['tdh'] = histogram / float(n)
                    self.data[label]['tdb'] = bins
                    #print '\tTDH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['tdh']
                except KeyError:
                    pass # This label does not have stv
                # fsh
                try:
                    values = self.data[label]['fsv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['fsh'] = histogram / float(n)
                    self.data[label]['fsb'] = bins
                    #print '\tFSH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['fsh']
                except KeyError:
                    pass # This label does not have stv
                # ssh
                try:
                    values = self.data[label]['ssv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['ssh'] = histogram / float(n)
                    self.data[label]['ssb'] = bins
                    #print '\tSSH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['ssh']
                except KeyError:
                    pass # This label does not have stv
                # tsh
                try:
                    values = self.data[label]['tsv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['tsh'] = histogram / float(n)
                    self.data[label]['tsb'] = bins
                    #print '\tTSH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['tsh']
                except KeyError:
                    pass # This label does not have stv
                # rv
                try:
                    values = self.data[label]['rv']

                    n = len(values)
                    nbins = 30
                    (histogram, bins) = np.histogram(values, nbins)
                    self.data[label]['rh'] = histogram / float(n)
                    self.data[label]['rb'] = bins
                    #print '\tRH. Values: {}'.format(values)
                    #print '\tNbins: {}'.format(nbins)
                    #print self.data[label]['rh']
                except KeyError:
                    pass # This label does not have stv
                values = self.data[label]['rv']


            self.data[label] = label_dict

        except Exception as inst:
            print 'Problem in compute_histograms() in class histograms'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)

    def __repr__(self):
        """
        print as an R vector
        """
        print 'Histograms'
        for label in self.data:
            print label
            for hist in self.data[label]:
                print '\t',hist, self.data[label][hist]
        return "" 





class model():
    """
    This model contains all the info and operations about one tuple
    """
    def __init__(self):
        # The parent state model
        self.parent = -1
        # The tuple we are going to store information about
        self.tuple = ""
        #self.T1 = timedelta(seconds=0)
        self.first_T1 = 0.0
        #self.T1 = 0.0
        self.T1 = -1
        self.start_timestamp = False
        self.previous_timestamp = -1
        self.current_timestamp = -1
        self.state = ""
        # Dictionary to hold the labels assigned to this tuple
        self.ground_truth_labels = {}
        self.time_difference = -1
        self.predicted_label = "Unknown"
        self.probability_of_predicted_label = -1
        self.start_time = -1
        # All the ids of the netflows in this tuple
        self.netflows_ids = []
        # All the labels for this tuple are stored here
        self.labels = {}
        self.best_label_for_this_tuple = ""

        self.T1_to_store = -1
        self.T2_to_stere = -1

        # Statistical values
        ####################
        #self.TD = timedelta(seconds=0)
        self.TD = -1
        self.TDs = []
        self.TD_mean = -1
        self.TD_std = -1
        self.TD_median = -1
        # Only with T2 we got all the values, because T1 is T2 after one flow
        #self.T2 = timedelta(seconds=0)
        self.first_T2 = 0.0
        #self.T2 = 0.0
        self.T2 = -1
        self.T2s = []
        self.T2s_mean = -1
        self.T2s_std = -1
        self.T2s_median = -1
        # Bytes
        self.bytes = 0 # Last bytes
        self.byteses = []
        self.total_bytes = -1
        self.bytes_mean = -1
        self.bytes_std = -1
        self.bytes_median = -1
        # Duration
        self.duration = -1 # Last duration
        self.durations = []
        self.total_duration = -1
        self.duration_mean = -1
        self.duration_std = -1
        self.duration_median = -1
        # Src Bytes to Dst Bytes ratio
        self.srcdstbytesratio = 0
        self.srcdstbytesratios = []
        self.srcdstbytesratio_mean = -1
        self.srcdstbytesratio_std = -1
        self.srcdstbytesratio_median = -1

        # Relation between packets/bytes
        # Relation between sent and received bytes.
        self.rel = -1
        self.rels = []
        self.rel_mean = -1
        self.rel_std = -1
        self.rel_median = -1

        # State
        self.amount_of_each_states = {}



    def set_predicted_label(self,label):
        """
        Store the predicted label
        """
        self.predicted_label = label

    def set_predicted_probability(self,probability):
        """
        Store the predicted probability
        """
        self.probability_of_predicted_label = probability

    def get_predicted_label(self):
        """
        Get the predicted label
        """
        return self.predicted_label

    def get_predicted_probability(self):
        """
        Get the predicted probability
        """
        return self.probability_of_predicted_label

    def get_state(self):
        """
        Returns the state
        """
        return self.state


    def add_TD(self,TD):
        """
        Adds a new TD
        """
        try:
            self.TDs.append(TD)

        except Exception as inst:
            print 'Problem in add_TD() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def add_timestamp(self,starttime):
        """
        Gets a new timestamp, compute all the time diferences
        """
        try:

            # T1 is replaced by T2
            self.T1 = self.T2

            # Try both formats
            try:
                self.current_timestamp = datetime.strptime(starttime, '%Y/%m/%d %H:%M:%S.%f')
            except ValueError:
                self.current_timestamp = datetime.strptime(starttime, '%Y-%m-%d %H:%M:%S.%f')

            if not self.start_timestamp:
                self.start_timestamp = self.current_timestamp

            # T2 is the new difference between the current timestamp and the previous one
            try:
                tdiff = self.current_timestamp - self.previous_timestamp
                self.T2 = tdiff.total_seconds()

            except TypeError:
                # The previous timestamp is still -1 and not any real time... Should be the first flow to arrive...
                #self.T2 = 0.0
                self.T2 = -1

            # Store the seconds
            self.T2s.append(self.T2)

            # The previous timestamp is now the current one
            self.previous_timestamp = self.current_timestamp

            # For the simulations of flows
            # Store the first T1 and T2 that has a real value. Later we put them on disk along the MC.
            if len(self.netflows_ids) == 3:
                self.T1_to_store = self.T1
                self.T2_to_store = self.T2

        except Exception as inst:
            print 'Problem in add_timestamp() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def add_label(self,label):
        """
        Store the new label
        """
        try:
            try:
                amount = self.labels[label]
                # We have this label
                amount += 1
            except KeyError:
                # We don't have this label
                amount = 1
            self.labels[label] = amount

        except Exception as inst:
            print 'Problem in add_label() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def compute_new_state(self):
        """
        Compute a new state letter from all the new data
        """
        try:
            periodic = -2
            size = -2
            length = -2

            if debug > 6:
                print 'Computing a new state for tuple {} ({}). T1={}, T2={}'.format(self.tuple, self.best_label_for_this_tuple, self.T1, self.T2)

            # Also compute the best label
            self.compute_best_label_so_far()

            # Compute the features

            if self.T1 == -1 or self.T2 == -1:
                periodic = -1
            elif self.T2 >= self.timeout_threshold.seconds:
                # 0 means timeout
                self.state += '0'

            # Periodicity
            if self.T1 != -1 and self.T2 != -1:
                # Original idea of the difference
                self.oldTD = self.T2 - self.T1


                # New idea of the division
                ##########################
                try:
                    # The numerator should be bigger than the denominator, so we have all the values > 1
                    if self.T2 >= self.T1:
                        self.TD = self.T2 / float(self.T1)
                    else:
                        self.TD = self.T1 / float(self.T2)
                except ZeroDivisionError:
                    if debug > 9:
                        print 'WARNING! Division by zero while computing the TD. T1 is zero! That means no delay?'
                    self.TD = 1
                ##########################


                if debug > 9:
                    print 'New division TD: {:>5.20f} (Old: {}). T2={}, T1={}'.format(self.TD,self.oldTD,self.T2,self.T1)

                # add the TD to the model
                self.add_TD(self.TD)


                if debug > 9:
                    print 'Actual TD: {} (old: {}), Th1: {}, Th2: {}, Th3: {}. Duration: {}, Size: {}'.format(self.TD, self.oldTD, self.time_threshold_1.total_seconds(), self.time_threshold_2.total_seconds(), self.time_threshold_3.total_seconds(), self.duration, self.bytes)

                # Strong periodicity
                if self.TD <= self.time_threshold_1.total_seconds():
                    periodic = 1
                # Weak periodicity
                elif self.TD < self.time_threshold_2.total_seconds():
                    periodic = 2
                # Weak not periodicity
                elif self.TD < self.time_threshold_3.total_seconds():
                    periodic = 3
                # Not periodicity
                else:
                    periodic = 4

            # Out of the if. Now this applies to all the flows, not only to those with some T1 or T2.


            # Size
            if self.bytes <= self.bytes_threshold_1:
                size = 1
            elif self.bytes > self.bytes_threshold_1 and self.bytes <= self.bytes_threshold_2:
                size = 2
            elif self.bytes > self.bytes_threshold_2:
                size = 3

            # Duration
            if self.duration <= self.duration_threshold_1.total_seconds():
                length = 1
            elif self.duration > self.duration_threshold_1.total_seconds() and self.duration <= self.duration_threshold_2.total_seconds():
                length = 2
            elif self.duration > self.duration_threshold_2.total_seconds():
                length = 3
        
            if debug > 9:
                print '\t> Periodicity:{}, Size:{}, Length:{}'.format(periodic, size, length)


            # If we are in Threshold operational mode and the flow HAS at least some periodicity we print the values for training the threshold.
            # We do not print the flows without periodicity because we want to train the periodicity.
            if 'Thresholds' in self.operation_mode and self.TD != -1:
                if 'Botnet' in self.best_label_for_this_tuple:
                    #print '{:10.10f},{:10.10f},{:10.10f},{},{:10.10f},Botnet'.format(self.TD,self.T2,self.T1,self.bytes,self.duration)
                    print '{:10.10f},{},{:10.10f},Botnet'.format(self.TD,self.bytes,self.duration)
                elif 'Normal' in self.best_label_for_this_tuple:
                    #print '{:10.10f},{:10.10f},{:10.10f},{},{:10.10f},Normal'.format(self.TD,self.T2,self.T1,self.bytes,self.duration)
                    print '{:10.10f},{},{:10.10f},Normal'.format(self.TD,self.bytes,self.duration)

            # Compute the new state
            if periodic == -1:
                if size == 1:
                    if length == 1:
                        self.state += '1'
                    elif length == 2:
                        self.state += '2'
                    elif length == 3:
                        self.state += '3'
                elif size == 2:
                    if length == 1:
                        self.state += '4'
                    elif length == 2:
                        self.state += '5'
                    elif length == 3:
                        self.state += '6'
                elif size == 3:
                    if length == 1:
                        self.state += '7'
                    elif length == 2:
                        self.state += '8'
                    elif length == 3:
                        self.state += '9'
            elif periodic == 1:
                if size == 1:
                    if length == 1:
                        self.state += 'a'
                    elif length == 2:
                        self.state += 'b'
                    elif length == 3:
                        self.state += 'c'
                elif size == 2:
                    if length == 1:
                        self.state += 'd'
                    elif length == 2:
                        self.state += 'e'
                    elif length == 3:
                        self.state += 'f'
                elif size == 3:
                    if length == 1:
                        self.state += 'g'
                    elif length == 2:
                        self.state += 'h'
                    elif length == 3:
                        self.state += 'i'
            elif periodic == 2:
                if size == 1:
                    if length == 1:
                        self.state += 'A'
                    elif length == 2:
                        self.state += 'B'
                    elif length == 3:
                        self.state += 'C'
                elif size == 2:
                    if length == 1:
                        self.state += 'D'
                    elif length == 2:
                        self.state += 'E'
                    elif length == 3:
                        self.state += 'F'
                elif size == 3:
                    if length == 1:
                        self.state += 'G'
                    elif length == 2:
                        self.state += 'H'
                    elif length == 3:
                        self.state += 'I'
            elif periodic == 3:
                if size == 1:
                    if length == 1:
                        self.state += 'r'
                    elif length == 2:
                        self.state += 's'
                    elif length == 3:
                        self.state += 't'
                elif size == 2:
                    if length == 1:
                        self.state += 'u'
                    elif length == 2:
                        self.state += 'v'
                    elif length == 3:
                        self.state += 'w'
                elif size == 3:
                    if length == 1:
                        self.state += 'x'
                    elif length == 2:
                        self.state += 'y'
                    elif length == 3:
                        self.state += 'z'
            elif periodic == 4:
                if size == 1:
                    if length == 1:
                        self.state += 'R'
                    elif length == 2:
                        self.state += 'S'
                    elif length == 3:
                        self.state += 'T'
                elif size == 2:
                    if length == 1:
                        self.state += 'U'
                    elif length == 2:
                        self.state += 'V'
                    elif length == 3:
                        self.state += 'W'
                elif size == 3:
                    if length == 1:
                        self.state += 'X'
                    elif length == 2:
                        self.state += 'Y'
                    elif length == 3:
                        self.state += 'Z'
            if debug > 9:
                print '\tState assigned: {}'.format(self.state[-1])
            self.parent.histogram.add_point(self.get_best_label_so_far(), self.state[-1], self.TD, self.duration, self.bytes, self.srcdstbytesratio )

        except Exception as inst:
            print 'Problem in compute_new_state() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)

    def get_best_label_so_far(self):
        """
        Return the best label so far
        """
        self.compute_best_label_so_far()
        return self.best_label_for_this_tuple

    def compute_best_label_so_far(self):
        """
        From all the ground truth labels assigned to this tuple, get the best bet.
        """
        try:
                sorted_labels = sorted(self.labels.iteritems(), key=operator.itemgetter(1))
                if sorted_labels:
                    self.best_label_for_this_tuple = sorted_labels[-1][0]

                    # If the best label is Attempt but the second best label has CC, then change to CC, because some CC
                    # also have attempts and it can be sometimes that the attempts are more than the CC, but we care about the CC.
                    if 'Attempt' in self.best_label_for_this_tuple:
                        try:
                            second_best_label_for_this_tuple = sorted_labels[-2][0]

                            # We don't want to change a CC label if it changes to Attempt
                            if 'CC' in second_best_label_for_this_tuple:
                                self.best_label_for_this_tuple = second_best_label_for_this_tuple

                        except IndexError:
                            pass

        except Exception as inst:
            print 'Problem in compute_best_label_so_far() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)



    def print_this_tuple_so_far(self):
        """
        Print the information for this tuple until now
        """
        try:

            global without_colors

            # Replace the ' ' with _ in the time difference, so the output can be separated with spaces later if needed
            T1_t = '{}'.format(self.T1)
            T2_t = '{}'.format(self.T2)
            T1 = T1_t.replace(' ', '_')
            T2 = T2_t.replace(' ', '_')
            if 'csv' not in self.print_mode and 'epoch' in self.print_mode:
                print '{},\t{},\tT1={}, T2={}, TD={:6.1f}, Bytes={:5}, BytesRatio={:<6.4}, Dur={:.3f}, State={}, Label={}'.format(self.tuple, time.mktime(self.current_timestamp.timetuple()), T1, T2, self.TD, self.bytes, self.srcdstbytesratio, self.duration, self.state, self.best_label_for_this_tuple)
            elif 'csv' not in self.print_mode and 'epoch' not in self.print_mode:
                #print '{},\t{},\tT1={}, T2={}, TD={:6.1f}, Bytes={:5}, BytesRatio={:<6.4}, Dur={:.3f}, State={}, Label={}'.format(self.tuple, self.current_timestamp, T1, T2, self.TD, self.bytes, self.srcdstbytesratio, self.duration, self.state, self.best_label_for_this_tuple)
                print '{},\t{},\tT1={:5.10f}, T2={:5.10f}, TD={:6.10f}, Bytes={:5}, BytesRatio={:<6.4}, Dur={:.3f}, State={}, Label={}'.format(self.tuple, self.current_timestamp, self.T1, self.T2, self.TD, self.bytes, self.srcdstbytesratio, self.duration, self.state, self.best_label_for_this_tuple)
            # 'timestamp,id,tuple,label,bytesMedian,bytesMin,bytesMax,srcdstbytesratio,durationMedian,durationMin,durationMax,timediff,TDMedian,TDMin,TDMax'
            elif 'csv' in self.print_mode and 'epoch' not in self.print_mode:
                print '{},{},{},{},{},{},{},{}'.format(self.current_timestamp, self.id, self.tuple, self.best_label_for_this_tuple, self.bytes, self.srcdstbytesratio, self.duration, self.T2)
                #print '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(self.current_timestamp, self.id, self.tuple, self.best_label_for_this_tuple, self.bytes_median, np.min(self.byteses), np.max(self.byteses), self.srcdstbytesratio_median, self.duration_median, np.min(self.durations), np.max(self.durations), self.T2, self.TD_median, np.min(self.TDs), np.max(self.TDs))
            elif 'csv' in self.print_mode and 'epoch' in self.print_mode:
                print '{},{},{},{},{},{},{},{}'.format(time.mktime(self.current_timestamp.timetuple()), self.id, self.tuple, self.best_label_for_this_tuple, self.bytes, self.srcdstbytesratio, self.duration, self.T2)
                #print '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(time.mktime(self.current_timestamp.timetuple()), self.id, self.tuple, self.best_label_for_this_tuple, self.bytes_median, np.min(self.byteses), np.max(self.byteses), self.srcdstbytesratio_median, self.duration_median, np.min(self.durations), np.max(self.durations), self.T2, self.TD_median, np.min(self.TDs), np.max(self.TDs) )


        except Exception as inst:
            print 'Problem in print_this_tuple_so_far() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def compute_statistics(self):
        """
        Compute the final statistics for this model
        """
        try:
            # SrcBytes
            self.srcdstbytesratio_mean = np.mean(self.srcdstbytesratios)
            self.srcdstbytesratio_std = np.std(self.srcdstbytesratios)
            self.srcdstbytesratio_median = np.median(self.srcdstbytesratios)

            # Bytes
            self.total_bytes = np.sum(self.byteses)
            self.bytes_mean = np.mean(self.byteses)
            self.bytes_std = np.std(self.byteses)
            self.bytes_median = np.median(self.byteses)

            # Duration
            self.total_duration = np.sum(self.durations)
            self.duration_mean = np.mean(self.durations)
            self.duration_std = np.std(self.durations)
            self.duration_median = np.median(self.durations)

            # T2
            self.T2s_mean = np.mean(self.T2s)
            self.T2s_std = np.std(self.T2s)
            self.T2s_median = np.median(self.T2s)

            # TD
            self.TD_mean = np.mean(self.TDs)
            self.TD_std = np.std(self.TDs)
            self.TD_median = np.median(self.TDs)

            # Relation between packets/bytes
            self.rel_mean = np.mean(self.rels)
            self.rel_std = np.std(self.rels)
            self.rel_median = np.median(self.rels)
        except Exception as inst:
            print 'Problem in compute_statistics() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def print_this_tuple_final(self):
        """
        Print the information for this tuple
        """
        try:
            global debug

            time_diff_temp = self.current_timestamp-self.start_timestamp
            time_diff = str(time_diff_temp).replace(' ', '_')

            if 'normal' in self.print_mode and 'csv' not in self.print_mode:
                # This is printed as one line! The only thing missing is the long state.
                print '\n{}'.format(self.tuple)
                print 'Time:\t\t {}->{}. TimeSpan: {} days, {} hours, {} min.'.format(self.start_timestamp, self.current_timestamp, time_diff_temp.days, time_diff_temp.total_seconds()/60/60, time_diff_temp.total_seconds()%60 )
                print 'Duration:\t {:.3f}s ({:.3f} m). Mean:{:.3f}s. Std:{:.3f}. Median:{:.3f}s. Max: {:.3f}s. Min: {:.3f}s'.format(self.total_duration, self.total_duration/60.0, self.duration_mean, self.duration_std, self.duration_median, np.max(self.durations), np.min(self.durations) )
                print 'Total Size:\t {:.3f} B ({:.3f} KB). Mean:{:.3f} B. Std:{:.3f}. Median:{:.3f} B ({:3f}KB)'.format(self.total_bytes, self.total_bytes/1024.0, self.bytes_mean, self.bytes_std, self.bytes_median, self.bytes_median/1024)
                print 'Size Ratio Mean: {:.3f} %. Std:{:.3f}. Median:{:.3f} %'.format(self.srcdstbytesratio_mean, self.srcdstbytesratio_std, self.srcdstbytesratio_median)
                print 'Freq: Mean:\t {:.3f}s. Std:{:.3f}. Median:{:.3f}s'.format(self.T2s_mean, self.T2s_std, self.T2s_median)
                print 'TD: Mean:\t {:.3f}s. Std:{:.3f}. Median:{:.3f}s'.format(self.TD_mean, self.TD_std, self.TD_median)
                print '#flows:\t\t {}'.format(len(self.netflows_ids))
                #print 'State: {}'.format(self.state)
                print 'Label:\t\t {}'.format(self.best_label_for_this_tuple)
                #import nltk
                #state_frequency = nltk.FreqDist(letter for letter in self.state)
                #print state_frequency
            elif 'csv' in self.print_mode:
                # timestamp,id,tuple,label,bytes,duration,timediff
                print '{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}'.format(time.mktime(self.current_timestamp.timetuple()), self.id, self.tuple, self.best_label_for_this_tuple, self.bytes_median, np.min(self.byteses), np.max(self.byteses), self.srcdstbytesratio_median, self.duration_median, np.min(self.durations), np.max(self.durations), self.T2, self.TD_median, np.min(self.TDs), np.max(self.TDs) )
            elif 'oneline' in self.print_mode and not 'epoch' in self.print_mode:
                print '{}\t{}\tTimeDiff:{}\tState:{}\t#flows:{}\tDurMedian:{:.3f}s\tDurMin:{:.3f}s\tDurMax:{:.3f}s\tSizeMedian:{:.3f}B\tSizeMin:{:.3f}B\tSizeMax:{:.3f}B\tBytesRatioMed:{:.5f}\tFreqMed:{:.3f}s\tTDMedian:{:.3f}s\tTDMin:{:3f}s\tTDMax:{:3f}s\tPacketsRel:{}\tLabel:{}'.format(self.start_timestamp, self.tuple, time_diff, self.state, len(self.netflows_ids), self.duration_median, np.min(self.durations), np.max(self.durations), self.bytes_median, np.min(self.byteses), np.max(self.byteses), self.srcdstbytesratio_median, self.T2s_median, self.TD_median, np.min(self.TDs), np.max(self.TDs), self.rel_median, self.best_label_for_this_tuple)
            elif 'shortline' in self.print_mode and not 'epoch' in self.print_mode:
                print '{}\t{}\tTimeDiff:{}\tState:{}\t#flows:{}\tLabel:{}'.format(self.start_timestamp, self.tuple, time_diff, self.state, len(self.netflows_ids), self.best_label_for_this_tuple)
            elif 'shortline' in self.print_mode and 'epoch' in self.print_mode:
                print '{}\t{}\tTimeDiff:{}\tState:{}\t#flows:{}\tLabel:{}'.format(time.mktime(self.start_timestamp.timetuple()), self.tuple, time_diff, self.state, len(self.netflows_ids), self.best_label_for_this_tuple)
            elif 'oneline' in self.print_mode and 'epoch' in self.print_mode:
                print '{}\t{}\tTimeDiff:{}\tState:{}\t#flows:{}\tDurMedian:{:.3f}s\tDurMin:{:.3f}s\tDurMax:{:.3f}s\tSizeMedian:{:.3f}B\tSizeMin:{:.3f}B\tSizeMax:{:.3f}B\tBytesRatioMed:{:.5f}\tFreqMed:{:.3f}s\tTDMedian:{:.3f}s\tTDMin:{:3f}s\tTDMax:{:3f}s\tPacketsRel:{}\tLabel:{}'.format(time.mktime(self.start_timestamp.timetuple()), self.tuple, time_diff, self.state, len(self.netflows_ids), self.duration_median, np.min(self.durations), np.max(self.durations), self.bytes_median, np.min(self.byteses), np.max(self.byteses), self.srcdstbytesratio_median, self.T2s_median, self.TD_median, np.min(self.TDs), np.max(self.TDs), self.rel_median, self.best_label_for_this_tuple)

        except Exception as inst:
            print 'Problem in print_this_tuple_final() in class model'
            print 'Current tuple: {}'.format(self.tuple)
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def add_bytes(self,bytes, srcbytes):
        """
        Add the bytes
        """
        try:
            self.bytes = bytes
            self.byteses.append(bytes)
            if bytes != 0 and srcbytes != -1:
                self.srcdstbytesratio = float(srcbytes)/bytes
                self.srcdstbytesratios.append(float(srcbytes)/bytes)
            else:
                self.srcdstbytesratio = 0
                self.srcdstbytesratios.append(0)

        except Exception as inst:
            print 'Problem in add_bytes() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)

    def add_packets_bytes_rel(self,rel):
        """
        Add the relationship between packets and bytes
        """
        try:
            self.rels.append(rel)
            self.rel = rel

        except Exception as inst:
            print 'Problem in add_packets_bytes_rel() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)

    def add_duration(self,duration):
        """
        Add the duration
        """
        try:
            self.durations.append(duration)
            self.duration = duration

        except Exception as inst:
            print 'Problem in add_duration() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def add_netflow_id(self,id):
        """
        Add the duration
        """
        try:
            self.netflows_ids.append(int(id))
        except Exception as inst:
            print 'Problem in add_neflow_id() in class model'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


class stateModels:
    """
    This class handles all the models
    """
    def __init__(self):
        # To hold all the models
        #group_of_models = []
        # Hold all the original netflows
        self.original_netflows = {}
        # A dictionary to hold all the tuples and its model instances
        self.tuples_dict = {}
        self.tuples_dict_training = {}
        self.tuples_dict_validation = {}
        self.tuple_to_analyze = ""
        self.label_to_analyze = ""
        # Statistical values
        ####################
        self.TD_means = []
        self.TD_stds = []
        self.TD_medians = []
        self.TD_mean = 0
        self.TD_std = 0
        self.TD_median = 0
        self.T2_means = []
        self.T2_stds = []
        self.T2_medians = []
        self.T2_mean = 0
        self.T2_std = 0
        self.T2_median = 0
        self.duration_means = []
        self.duration_stds = []
        self.duration_medians = []
        self.duration_mean = 0
        self.duration_std = 0
        self.duration_median = 0
        self.bytes_means = []
        self.bytes_stds = []
        self.bytes_medians = []
        self.bytes_mean = 0
        self.bytes_std = 0
        self.bytes_median = 0

        # Relation between packets/bytes
        self.rel_means = []
        self.rel_stds = []
        self.rel_medians = []
        self.rel_mean = 0
        self.rel_std = 0
        self.rel_median = 0
        # Src Dst Bytes ratio
        self.srcdstbytesratio_means = []
        self.srcdstbytesratio_stds = []
        self.srcdstbytesratio_medians = []
        self.srcdstbytesratio_mean = 0
        self.srcdstbytesratio_std = 0
        self.srcdstbytesratio_median = 0

        # Create the histogram object
        self.histogram = histograms()
       

    def compute_metrics(self, labeled_netflowFile):
        """
        Compute the metrics after
        """
        try:
            if debug > 1:
                print 'Computing the metrics for the labeled netflow file {}'.format(labeled_netflowFile)

            # Get the header
            subprocess.Popen("head -n 1 "+labeled_netflowFile+" > header", shell=True, stdout=subprocess.PIPE).stdout.read().strip()
            # Sort it
            devnull = open(os.devnull, 'w')
            p = subprocess.Popen("cat "+labeled_netflowFile+"|grep -v Start |sort > "+labeled_netflowFile+".sorted.temp", shell=True, stdout=devnull)
            p.wait()
            #p = subprocess.Popen("echo "+header+" | cat - "+labeled_netflowFile+".sorted.temp > "+labeled_netflowFile+".sorted" , shell=True, stdout=devnull)
            p.wait()
            p = subprocess.Popen("cat header "+labeled_netflowFile+".sorted.temp > "+labeled_netflowFile+".sorted" , shell=True, stdout=devnull)
            p.wait()
            subprocess.Popen("rm "+labeled_netflowFile+".sorted.temp" , shell=True, stdout=devnull)
            subprocess.Popen("rm header" , shell=True, stdout=devnull)
            devnull.close()

            # BotnetDetectorsComparer.py
            devnull = open(os.devnull, 'w')
            if debug == 0:
                p = subprocess.Popen("BotnetDetectorsComparer.py -f "+labeled_netflowFile+".sorted -T 300 -a 0.01 -t weight -c output-"+str(self.min_probability_threshold)+".csv ", shell=True, stdout=devnull)
            else:
                p = subprocess.Popen("BotnetDetectorsComparer.py -f "+labeled_netflowFile+".sorted -T 300 -a 0.01 -t weight -c output-"+str(self.min_probability_threshold)+".csv -D "+str(debug), shell=True)
            p.wait()
            devnull.close()

            # Read the csv output
            # Name,t_TP,t_TN,t_FP,t_FN,t_TPR,t_TNR,t_FPR,t_FNR,t_Precision,t_Accuracy,t_ErrorRate,t_fmeasure1,t_fmeasure2,t_fmeasure05,t_B1,t_B2,t_B3,t_B4,t_B5
            output = open('output-'+str(self.min_probability_threshold)+'.csv', 'r')
            for line in output:
                if 'CCDetector' in line:
                    ccdetector_line = line
                    break
            results = {}
            results['t_TP'] = float(line.split(',')[1])
            results['t_TN'] = float(line.split(',')[2])
            results['t_FP'] = float(line.split(',')[3])
            results['t_FN'] = float(line.split(',')[4])
            results['t_TPR'] = float(line.split(',')[5])
            results['t_TNR'] = float(line.split(',')[6])
            results['t_FPR'] = float(line.split(',')[7])
            results['t_FNR'] = float(line.split(',')[8])
            results['t_Precision'] = float(line.split(',')[9])
            results['t_Accuracy'] = float(line.split(',')[10])
            results['t_Error_Rate'] = float(line.split(',')[11])
            results['t_fmeasure1'] = float(line.split(',')[12])
            results['t_fmeasure2'] = float(line.split(',')[13])
            results['t_fmeasure05'] = float(line.split(',')[14])
            results['t_B1'] = float(line.split(',')[15])
            results['t_B2'] = float(line.split(',')[16])
            results['t_B3'] = float(line.split(',')[17])
            results['t_B4'] = float(line.split(',')[18])
            results['t_B5'] = float(line.split(',')[19])
            results['t_MCC'] = float(line.split(',')[20])

            if debug:
                print ' t_TP:{}, t_TN:{}, t_FP:{}, t_FN:{}, t_TPR:{}, t_TNR:{}, t_FPR:{}, t_FNR:{}, t_Precision:{}, t_Accuracy:{}, t_ErrorRate:{}, t_fmeasure1:{}, t_fmeasure2:{}, t_fmeasure05:{}, t_B1:{}, t_B2:{}, t_B3:{}, t_B4:{}, t_B5:{}, MCC:{}'.format(results['t_TP'], results['t_TN'], results['t_FP'], results['t_FN'], results['t_TPR'], results['t_TNR'], results['t_FPR'], results['t_FNR'], results['t_Precision'], results['t_Accuracy'], results['t_Error_Rate'], results['t_fmeasure1'], results['t_fmeasure2'], results['t_fmeasure05'], results['t_B1'], results['t_B2'], results['t_B3'], results['t_B4'], results['t_B5'], results['t_MCC'])

            output.close()
            
            return results

        except Exception as inst:
            print 'Problem in compute_metrics() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)
            

    def train_and_validate(self, netflowFile):
        """
        1- We separate all the 4-tuples in training and validation. The separation is ten-fold
        2- For each threshold set we do
         2.1- Try the 
        """
        try:
            if debug:
                print 'Training and Validation phase'
                print 'Total amount of tuples: {}'.format(len(self.tuples_dict))

            tuples_keys = np.array(self.tuples_dict.keys())
            kf = KFold(len(tuples_keys), n_folds=self.number_of_folds, indices=False)
            

            total_results = {}

            # Use different thresholds values to validate the model
            while self.min_probability_threshold <= self.max_probability_threshold:
                if debug:
                    print 'Validating threshold:{} (max {})'.format(self.min_probability_threshold, self.max_probability_threshold)
                # Store the results of this run of this threshold
                run_results = []

                for train, test in kf:
                    # These are the tuples names
                    training_tuples, validation_tuples = tuples_keys[train], tuples_keys[test]

                    # Now get the dicts
                    for tuple in training_tuples:
                        self.tuples_dict_training[tuple] = self.tuples_dict[tuple]
                    for tuple in validation_tuples:
                        self.tuples_dict_validation[tuple] = self.tuples_dict[tuple]

                    # Here we should train the markov chains
                    self.create_markov_chains('Training')

                    # Here we should validate the model
                    self.predict_labels_with_markov_chains('Training')

                    # Output the labeled netflow file
                    self.output_labeled_netflows(netflowFile + '.labeled')

                    # Compute the metrics
                    results = self.compute_metrics(netflowFile + '.labeled')
                    run_results.append(results.values())

                    # Delete the Training folder or the next round
                    devnull = open(os.devnull, 'w')
                    p=subprocess.Popen("rm -rf Training", shell=True, stdout=devnull)
                    p.wait()
                    devnull.close()
                    self.tuples_dict_training = {}
                    self.tuples_dict_validation = {}

                # Summarize the results
                tr = np.array(run_results)
                
                total_results[self.min_probability_threshold] = tr.mean(axis=0)
                if debug:
                    #print '\nThreshold:{}'.format(self.min_probability_threshold)
                    print ' Results for threshold: {}'.format(self.min_probability_threshold)
                    for i in range(0,len(results)-1):
                        print '  {}: {}'.format(results.keys()[i], total_results[self.min_probability_threshold][i]),
                    print
                    print

                # Go to next threshold
                self.min_probability_threshold += self.step_threshold
                #if debug:
                #    print 'Moving the threshold to: {}'.format(self.min_probability_threshold)
        
            # Now find out the best probability threshold.
            best_fm = -1
            best_threshold = -1
            for threshold in total_results:
                # I don't know how to automatically get the f_measure without fixing the column number
                fm1 = total_results[threshold][8]
                if fm1 >= best_fm:
                    best_fm = fm1
                    best_threshold = threshold
            
            print
            print 'The best threshold is {}, with results:'.format(best_threshold)
            for i in range(0,len(results)-1):
                print '\t\t{}: {}'.format(results.keys()[i], total_results[best_threshold][i]),
            print

        except Exception as inst:
            print 'Problem in train_and_validate() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def our_walk(self,P,v_state, prob_original_state):
        """
        Get a markov chain and a group of states, and compute the probability that this group was generated by this markov chain.
        Our computation is different of the normal one in:
        - If a transition of states is not in the MC, then we store the prob until this point, and we start again from the next state. This way we search for the sub-chain of states with the max prob on this MC.
        """
        try:
            global debug
            i = 0
            res = 0.0
            best_distance_so_far = 1000000.0

            # When we have two consecutives -inf probs, (2 no in the matrix) the res = 0 and we may compute the distance using a res=0. But res=0 also means the perfect probability, so we should differentiate between these two states
            # when the prob WAS computed and res = 0, and when the prob was still NOT computed and res = 0. We do that with valid variable.
            valid = False

            while i < len(v_state):
                try:
                    vector = [v_state[i], v_state[i+1]]
                    #wv = P.walk_probability(vector)
                    #prob = np.float32(math.exp(wv))

                    # The prob is logarithmic
                    prob = P.walk_probability(vector)

                    if debug > 6:
                        print '\t\tVector: {}. Prob:{:.10f}'.format(vector, prob)
                    # Here is our trick. If two letters are not in the matrix...
                    if prob == float('-inf') and valid:
                        if debug > 6:
                            print '\t\t\tThis transition is not in the matrix.'

                        # Only change the best_prob_so_far on the next pair that we can't find.
                        distance = abs(prob_original_state - res)
                        if debug > 7:
                            print '\t\t\tSubstring distance to original: {} (orig:{}, res:{})'.format(distance, prob_original_state, res)
                        if distance < best_distance_so_far:
                            if debug > 7:
                                print '\t\t\tThe distance {} is smaller than the best distance so far {}, so we change it.'.format(distance, best_distance_so_far)
                            best_distance_so_far = distance
                        #else:
                        # Reset the prob everytime!
                        #res = 0

                        # Since the last prob was -inf, the res value is not valid any more
                        valid = False

                    elif prob != float('-inf'):
                        # Normal MC computation. The probs of all the matching pair of letters so far, are summed.
                        res = res + prob
                        # Since we actually computed a prob, the value is valid.
                        valid = True

                    if debug > 6:
                        print '\t\t\tRes:{}. Best distance so far: {:.10f}'.format(res, best_distance_so_far)
                except IndexError:
                    # We are out of letters
                    break
                i += 1

            # Just in case the best one is the last one
            distance = abs(prob_original_state - res)
            if distance < best_distance_so_far:
                if debug > 7:
                    print '\t\t\tThe distance {} is smaller than the best distance so far {}, so we change it...'.format(distance, best_distance_so_far)
                best_distance_so_far = distance
            
            if debug > 6:
                print '\t\t\tFinal res: {}. Final distance: {}'.format(res, best_distance_so_far)
            return best_distance_so_far
    

        except Exception as inst:
            print 'Problem in our_walk() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def print_this_state_model_final(self):
        """
        Print all the statistics for all the models in this stateModel
        """
        try:
            global debug
    
            print
            print 'Whole model. #tuples: {}'.format(len(self.tuples_dict))
            print 'RunT: Mean:{:.3f}s ({:.3f}m), Std:{:.3f}, Median:{:.3f}s ({:.3f}m)'.format(self.duration_mean, self.duration_mean/60.0, self.duration_std, self.duration_median, self.duration_median/60.0)
            print 'Size: Mean:{:.3f}b ({:.3f}KB), Std:{:.3f}, Median:{:.3f}b ({:.3f}KB)'.format(self.bytes_mean, self.bytes_mean/1024.0, self.bytes_std, self.bytes_median, self.bytes_median/1024)
            print 'Size Ratio Mean:{:.3f}, Std:{:.3f}, Median:{:.3f}'.format(self.srcdstbytesratio_mean, self.srcdstbytesratio_std, self.srcdstbytesratio_median)
            print 'Freq: Mean:{:.3f}s, Std:{:.3f}, Median:{:.3f}s'.format(self.T2_mean, self.T2_std, self.T2_median)
            print 'TD: Mean:{:.3f}s, Std:{:.3f}, Median:{:.3f}s'.format(self.TD_mean, self.TD_std, self.TD_median)


        except Exception as inst:
            print 'Problem in print_this_state_model_final() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def compute_statistics(self):
        """
        Compute the statistics for this stateModel
        """
        try:
            global debug

            self.T2_mean = np.mean(self.T2_means)
            self.T2_mean = -1
            self.T2_std = np.mean(self.T2_stds)
            self.T2_median = np.mean(self.T2_medians)
            self.duration_mean = np.mean(self.duration_means)
            self.duration_std = np.mean(self.duration_stds)
            self.duration_median = np.mean(self.duration_medians)
            self.bytes_mean = np.mean(self.bytes_means)
            self.bytes_std = np.mean(self.bytes_stds)
            self.bytes_median = np.mean(self.bytes_medians)
            self.TD_mean = np.mean(self.TD_means)
            self.TD_std = np.mean(self.TD_stds)
            self.TD_median = np.mean(self.TD_medians)

            # Relation between packets/bytes
            if self.rel_means == []:
                self.rel_mean = -1
            else:
                self.rel_mean = np.mean(self.rel_means)

            if self.rel_stds == []:
                self.rel_std = -1
            else:
                self.rel_std = np.mean(self.rel_stds)

            if self.rel_medians == []:
                self.rel_median = -1
            else:    
                self.rel_median = np.mean(self.rel_medians)

            # Relation between src and dst bytes
            self.srcdstbytesratio_mean = np.mean(self.srcdstbytesratio_means)
            self.srcdstbytesratio_std = np.mean(self.srcdstbytesratio_stds)
            self.srcdstbytesratio_median = np.mean(self.srcdstbytesratio_medians)

        except Exception as inst:
            print 'Problem in compute_statistics() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def print_statistics(self):
        """
        Search what we should print, compute statistics and print the model of each tuple, compute stateModel statistics and print stateModel 
        """
        try:
            global debug

            for tuple in self.tuples_dict:
                # ignore tuples that the length of the state is shorter that the specified.
                model = self.get_model_for_tuple(tuple)
                if len(model.state.replace('0','')) >= self.state_length:
                    model.compute_statistics()
                    #if (not self.tuple_to_analyze and not self.label_to_analyze) or debug :
                    model.print_this_tuple_final()

                    self.T2_means.append(model.T2s_mean)
                    self.T2_stds.append(model.T2s_std)
                    self.T2_medians.append(model.T2s_median)
                    self.duration_means.append(model.duration_mean)
                    self.duration_stds.append(model.duration_std)
                    self.duration_medians.append(model.duration_median)
                    self.bytes_means.append(model.bytes_mean)
                    self.bytes_stds.append(model.bytes_std)
                    self.bytes_medians.append(model.bytes_median)
                    self.TD_means.append(model.TD_mean)
                    self.TD_stds.append(model.TD_std)
                    self.TD_medians.append(model.TD_median)
                    self.rel_means.append(model.rel_mean)
                    self.rel_stds.append(model.rel_std)
                    self.rel_medians.append(model.rel_median)
                    self.srcdstbytesratio_means.append(model.srcdstbytesratio_mean)
                    self.srcdstbytesratio_stds.append(model.srcdstbytesratio_std)
                    self.srcdstbytesratio_medians.append(model.srcdstbytesratio_median)

            # Now for all of them
            if self.tuple_to_analyze or self.label_to_analyze:
                self.compute_statistics()
                if 'csv' not in self.print_mode:
                    self.print_this_state_model_final()

            #print self.histogram

        except Exception as inst:
            print 'Problem in print_statistics() in class stateModels'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def output_labeled_netflows(self,labelednetflowFile):
        """
        Go through all the tuples and output the labeled netflows
        """
        try:
            global debug

            if debug > 1:
                print 'Outputing the labeled netflows.'
            try:
                if labelednetflowFile == '-.labeled':
                    # Means we are reading from stdin
                    f = sys.stdout
                else:
                    f = open(labelednetflowFile, 'w')
            except:
                print 'Some error opening the output file {}'.format(labelednetflowFile)

            # Old header
            #f.writelines('#StartTime,Dur,RunTime,Proto,SrcAddr,Sport,Dir,DstAddr,Dport,State,SrcJitter,DstJitter,sTos,dTos,TotPkts,TotBytes,Trans,Mean,StdDev,Rate,SIntPkt,SIntDist,SIntPktAct,SIntActDist,SIntPktIdl,SIntIdlDist,DIntPkt,DIntDist,DIntPktAct,DIntActDist,DIntPktIdl,DIntIdlDist,Label(Normal:CC:Background),CCDetector(Normal:CC:Unknown)\n')

            # Ready for comparison
            # Automatize if we should put the dTos or not!
            #f.writelines('#StartTime,Dur,Proto,SrcAddr,Sport,Dir,DstAddr,Dport,State,sTos,TotPkts,TotBytes,SrcBytes,Label(Normal:CC:Background),CCDetector(Normal:CC:Unknown)\n')
            f.writelines('#StartTime,Dur,Proto,SrcAddr,Sport,Dir,DstAddr,Dport,State,sTos,dTos,TotPkts,TotBytes,SrcBytes,Label(Normal:CC:Background),CCDetector(Normal:CC:Unknown)\n')

            for tup in self.tuples_dict:
                model = self.get_model_for_tuple(tup)
                for id in model.netflows_ids:
                    original_netflow = self.original_netflows[id]
                    if debug > 5:
                        print 'Netflow: {}, Predicted Label: {}'.format(original_netflow, model.get_predicted_label())
                    f.writelines(original_netflow +','+ model.get_predicted_label() + '\n')

            f.close()

        except Exception as inst:
            print 'Problem in output_labeled_netflows() in class labeler'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def get_model_for_tuple(self,tuple):
        """
        Receives a tuple and returns an instance for that tuple. If it is a new tuple, returns a new instance, if it is an old tuple, returns the current instance
        """
        # Is this tuple new?
        global model_id
        try:
            temp_model = self.tuples_dict[tuple]
            # No

        except KeyError:
            # Yes
            temp_model = model()
            temp_model.tuple = tuple
            temp_model.id = model_id
            model_id += 1
            self.tuples_dict[tuple] = temp_model

        return temp_model


    def add_netflow(self,netflowArray):
        """
        Receives a netflow and adds it to the model
        """
        try:
            global debug

            #if debug:
            #    print '\nAdding netflow: {} '.format(netflowArray)

            # Just in case they are not in the file
            runtime = -1
            srcbytes = -1

            for col in netflowArray:
                if 'StartTime' in col.keys()[0]:
                    starttime = str(col.values()[0])
                elif 'SrcAddr' in col.keys()[0]:
                    srcaddr = str(col.values()[0])
                elif 'Sport' in col.keys()[0]:
                    sport = str(col.values()[0])
                elif 'DstAddr' in col.keys()[0]:
                    dstaddr = str(col.values()[0])
                elif 'Dport' in col.keys()[0]:
                    dport = str(col.values()[0])
                elif 'TotBytes' in col.keys()[0]:
                    bytes = str(col.values()[0])
                elif 'Dur' in col.keys()[0]:
                    duration = str(col.values()[0])
                elif 'RunTime' in col.keys()[0]:
                    runtime = str(col.values()[0])
                elif 'Label' in col.keys()[0]:
                    label = str(col.values()[0]).replace('flow=', '')
                elif 'Proto' in col.keys()[0]:
                    proto = str(col.values()[0])
                elif 'State' in col.keys()[0]:
                    flow_state = str(col.values()[0])
                elif 'TotPkts' in col.keys()[0]:
                    packets = str(col.values()[0])
                elif 'SrcBytes' in col.keys()[0]:
                    srcbytes = str(col.values()[0])

            #if debug:
            #    print 'Stime:{}, sddr:{}, sport:{}, dddr:{}, dport:{}, bytes:{}, dur:{}, runtime:{}, label={}, proto={}, state={}, packets={}, srcbytes={}'.format(starttime, srcaddr, sport, dstaddr, dport, bytes, duration, runtime, label, proto, flow_state, packets, srcbytes)

            if self.tuple_mode == '3': 
                tuple = srcaddr+'-'+dstaddr+'-'+dport+'-'+proto

            # If we are only analyzing one tuple or one label only get that those. Labels can be substrings. 
            if (self.tuple_to_analyze and self.tuple_to_analyze != tuple) or (self.label_to_analyze and self.label_to_analyze not in label):
                return

            # Get the model
            model = self.get_model_for_tuple(tuple)

            # Add this model and tuple to the list of tuples in this stateModel
            self.tuples_dict[tuple] = model

            # Assign to the model, the threshold of the stateModel
            model.tuple = tuple
            model.time_threshold_1 = self.time_threshold_1
            model.time_threshold_2 = self.time_threshold_2
            model.time_threshold_3 = self.time_threshold_3
            model.bytes_threshold_1 = self.bytes_threshold_1
            model.bytes_threshold_2 = self.bytes_threshold_2
            model.duration_threshold_1 = self.duration_threshold_1
            model.duration_threshold_2 = self.duration_threshold_2
            model.timeout_threshold = self.timeout_threshold
            model.min_probability_threshold = self.min_probability_threshold
            model.max_probability_threshold = self.max_probability_threshold
            model.print_mode = self.print_mode
            model.operation_mode = self.operation_mode
            model.state_length = self.state_length
            model.models_to_include = self.models_to_include
            model.number_of_folds = self.number_of_folds
            model.step_threshold = self.step_threshold
            # Parent state
            model.parent = self

            # Add the id of this netflow to the model
            model.add_netflow_id(self.netflow_id)

            # Store the info provided by this netflow in the tuple
            model.add_timestamp(starttime)
            model.add_label(label)
            try:
                model.add_bytes(int(bytes), int(srcbytes))
            except ValueError:
                # Means that the bytes or srcbytes are not there. Some icmp does this.
                model.add_bytes(0, 0)

            model.add_duration(float(duration))
            # Packets
            try:
                model.add_packets_bytes_rel(float( packets)/float(bytes) )
            except ValueError:
                # Means that the bytes or packets are not there. Some icmp does this.
                model.add_packets_bytes_rel( 0 )

            model.compute_new_state()

            #if (self.tuple_to_analyze and debug) or (self.label_to_analyze and debug):
            if (self.tuple_to_analyze) or (self.label_to_analyze):
                model.print_this_tuple_so_far()

        except Exception as inst:
            print 'Problem in add_netflow() in class labeler'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)


    def predict_labels_with_markov_chains(self, folder):
        """
        This function reads the stored markov models and the netflows and predicts a label for each tuple
        """
        try:
            global debug

            if debug > 2:
                print 'Folder used for models: {}'.format(folder)

            # Read all the models
            mcmodels = {}
            list_of_files = os.listdir(folder)
            label_name = ""
           


            for file in list_of_files:
                try:
                    file_name = folder+'/'+file

                    # Ignore the background models, just in case some of them are there... Also ignore the histograms file
                    if 'Background' in file_name or 'histograms' in file_name:
                        continue

                    input = open(file_name, 'rb')
                    p = cPickle.load(input)
                    P = cPickle.load(input)
                    stored_state = cPickle.load(input)
                    t1_t2 = cPickle.load(input)
                    # Relation between packets/bytes
                    rel_median = cPickle.load(input)
                    p_original_state = np.float32(cPickle.load(input))
                    input.close()
                    label_name = file.split('.mcmodel')[0]
                    #if debug:
                    #    print 'File: {}'.format(file)
                    #    print '\tp:{}, P:{}, stored_state:{}, t1_t2:{}, rel_median:{}, p_original_state:{}'.format(p,P,stored_state, t1_t2, rel_median, p_original_state)
                    
                    # Store the models. Here we don't care about t1 and t2 or rel_median
                    mcmodels[label_name] = [p,P,stored_state,p_original_state]
                except:
                    print 'Error. The label {0} has no model stored.'.format(label_name)
            
            if not mcmodels:
                print 'Error. There is not models to read.'
                exit(-1)


            # For each tuple
            for tup in self.tuples_dict:
                model = self.get_model_for_tuple(tup)
                state = model.get_state()
                best_ground_truth_label_for_this_tup = model.get_best_label_so_far()

                # Don't compare a model that is too small
                if len(state.replace('0','')) < self.state_length:
                    continue

                if debug > 2:
                    print 'Tuple: {}, GroundTruth Label: {}, State={}'.format(tup, best_ground_truth_label_for_this_tup, state)

                # compute the prob agains all the models
                all_probabilities_for_this_tuple = {}
                for label in mcmodels:
                    probability = -1
                    p = mcmodels[label][0]
                    P = mcmodels[label][1]
                    stored_state = mcmodels[label][2]
                    prob_original_state = mcmodels[label][3]
                    if debug > 5:
                        print '\tTrying Label: {}. State:{}. Orig prob:{}'.format(label, stored_state, prob_original_state)


                    # Use the model to predict a label
                    #################################

                    # Crete a list
                    v_state = list(state)

                    # Only compare the tuple with the models that share the protocol and the state of the protocol, like Established or Attempt. 
                    #if 'TCP' in best_ground_truth_label_for_this_tup:
                    if 'TCP' in model.tuple.upper():
                        # It is tcp
                        if 'TCP' in label:
                            if not 'Attempt' in best_ground_truth_label_for_this_tup:
                                if not 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth TCP and Established'
                                    # It is established
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original
                                    

                            elif 'Attempt' in best_ground_truth_label_for_this_tup:
                                if 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth TCP and Attempt'
                                    # It is attempt
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original

                            else: 
                                # Both are tcp but not established nor attempt, so compare...
                                # Compute the prob that this state was generated with this model
                                dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                if debug > 5:
                                    print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                # Check if we should consider it or not based on the threshold.
                                if dist_with_original < self.min_probability_threshold:
                                    if debug > 3:
                                        print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                        print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                    # Store it
                                    all_probabilities_for_this_tuple[label] = dist_with_original

                    #elif 'UDP' in best_ground_truth_label_for_this_tup:
                    elif 'UDP' in model.tuple.upper():
                        # It is udp
                        if 'UDP' in label:
                            if not 'Attempt' in best_ground_truth_label_for_this_tup:
                                if not 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth UDP and Established'
                                    # It is established
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original

                            elif 'Attempt' in best_ground_truth_label_for_this_tup:
                                if 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth UDP and Attempt'
                                    # It is attempt
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original

                            else: 
                                # Both are udp but not established nor attempt, so compare...
                                # Compute the prob that this state was generated with this model
                                dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                if debug > 5:
                                    print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                # Check if we should consider it or not based on the threshold.
                                if dist_with_original < self.min_probability_threshold:
                                    if debug > 3: 
                                        print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                        print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                    # Store it
                                    all_probabilities_for_this_tuple[label] = dist_with_original

                    # ICMP
                    elif 'ICMP' in model.tuple.upper():
                        # It is ICMP
                        if 'ICMP' in label:
                            if not 'Attempt' in best_ground_truth_label_for_this_tup:
                                if not 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth ICMP and Established'
                                    # It is established
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original

                            elif 'Attempt' in best_ground_truth_label_for_this_tup:
                                if 'Attempt' in label:
                                    if debug > 5:
                                        print '\t\tBoth ICMP and Attempt'
                                    # It is attempt
                                    # Compute the prob that this state was generated with this model
                                    dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                    if debug > 5:
                                        print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                    # Check if we should consider it or not based on the threshold.
                                    if dist_with_original < self.min_probability_threshold:
                                        if debug > 3:
                                            print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                            print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                        # Store it
                                        all_probabilities_for_this_tuple[label] = dist_with_original

                            else: 
                                # Both are udp but not established nor attempt, so compare...
                                # Compute the prob that this state was generated with this model
                                # Compute the prob that this state was generated with this model
                                dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                if debug > 5:
                                    print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                # Check if we should consider it or not based on the threshold.
                                if dist_with_original < self.min_probability_threshold:
                                    if debug > 3:
                                        print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                        print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                    # Store it
                                    all_probabilities_for_this_tuple[label] = dist_with_original
                    # ARP
                    elif 'ARP' in model.tuple.upper():
                        # It is ARP
                        if 'ARP' in label:
                            # Both are udp but not established nor attempt, so compare...
                            # Compute the prob that this state was generated with this model
                                # Compute the prob that this state was generated with this model
                                dist_with_original = self.our_walk(P,v_state,prob_original_state)

                                if debug > 5:
                                    print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                                # Check if we should consider it or not based on the threshold.
                                if dist_with_original < self.min_probability_threshold:
                                    if debug > 3:
                                        print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                        print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                                    # Store it
                                    all_probabilities_for_this_tuple[label] = dist_with_original

                    else:
                        if debug > 5:
                            print 'The groundtruth is not TCP nor UDP'

                        # The label has not TCP or UDP, so carry on... Maybe there are not labels in the original flow
                        # Compute the prob that this state was generated with this model
                        dist_with_original = self.our_walk(P,v_state,prob_original_state)

                        if debug > 5:
                            print '\t\tDist with original: {}, Min prob threshold:{}'.format(dist_with_original, self.min_probability_threshold)
                        # Check if we should consider it or not based on the threshold.
                        if dist_with_original < self.min_probability_threshold:
                            if debug > 3:
                                print '\t\tFor label: {}, We read: p={} ,P={}, state={}'.format(label, p,P, stored_state)
                                print '\t\tLabel {}. Dist with original: {:.20f} (POriginal: {:.20f}, Prob: {:.20f})'.format(label, dist_with_original, prob_original_state, probability)
                            # Store it
                            all_probabilities_for_this_tuple[label] = dist_with_original
                    

                # End for label
                sorted_probabilities = sorted(all_probabilities_for_this_tuple.iteritems(), key=operator.itemgetter(1), reverse=False)
                if debug > 4:
                    print 'Sorted Probabilities: {}'.format(sorted_probabilities)


                try:
                    predicted_probability = sorted_probabilities[0][1]
                except IndexError:
                    predicted_probability = -1
                    
                # output the bigger if it is bigger that the probability threshold
                #if sorted_probabilities[0][1] > self.min_probability_threshold:
                try:
                    predicted_label = sorted_probabilities[0][0]
                    # If the best probability is 0, then just say that we don't know.
                    #if predicted_probability == 0.0:
                        #predicted_label = 'Unknown'
                except IndexError:
                    predicted_label = 'Unknown'


                # Store the predicted label and the probability
                model.set_predicted_label(predicted_label)
                model.set_predicted_probability(predicted_probability)

                if debug > 1:
                    if debug > 2:
                        for sp in sorted_probabilities:
                            print '\tDistance with label {0:50s}\t{1:.100f}'.format(sp[0],sp[1])
                    print '\tFinal label selected for label {}: {} ({})'.format(best_ground_truth_label_for_this_tup, model.get_predicted_label(), model.get_predicted_probability())
                    print


        except Exception as inst:
            print 'Problem in predict_labels_with_markov_chains()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            exit(-1)



    def create_markov_chains(self,folder):
        """
        This function creates the markov chains for each label and store them in a folder
        """
        try:
            global debug
            
            if debug > 2:
                print 'Folder used for models: {}'.format(folder)
            
            # Extract the uniq labels in the model
            uniq_labels = {}
            uniq_labels_for_t1t2 = {}
            uniq_labels_for_rel = {}
            uniq_labels_for_state = {}

            # For each tuple... what??? being trained?
            for tup in self.tuples_dict_training:
                # Get the model for that tuple
                model = self.get_model_for_tuple(tup)
                # Get the state for that model
                state = model.get_state()

                # Compute the mean of the packets/bytes relationships. The mean is ok for this.
                rel_mean = np.mean(model.rels)

                best_label_for_this_tup = model.get_best_label_so_far()
                if debug > 4:
                    print 'Extracting the unique labels for tuple: {}. Label: {}. Len of state: {}'.format(tup, best_label_for_this_tup, len(state))

                # If the length of the state is less than the limit. Stop
                if len(state.replace('0','')) < self.state_length:
                    continue

                # Don't create a model for all the background
                if 'Background' in best_label_for_this_tup :
                    continue
                # If we don't want all the models and the model wanted is not this one. Stop
                elif 'all' not in self.models_to_include and self.models_to_include not in best_label_for_this_tup:
                    continue
                # If we want all the models or the model wanted is this one. Carry on.
                elif 'all' in self.models_to_include or self.models_to_include in best_label_for_this_tup:
                    pass


                # All the same states for all tuples are stored together with the separator #
                try:
                    previous_state = uniq_labels[best_label_for_this_tup]

                    # Store the longest state for this label
                    if len(uniq_labels_for_state[best_label_for_this_tup]) < len(state):
                        uniq_labels_for_state[best_label_for_this_tup] = state

                    # it was there
                    new_state = previous_state+'#'+state

                    if debug > 4:
                        print '\tNew State for tuple {} : {}'.format(tup, new_state)

                    # Store the new state
                    uniq_labels[best_label_for_this_tup] = new_state

                    # Make a tuple with the t1 and t2 to store for this tup
                    temp_T1andT2 = (model.T1_to_store, model.T2_to_store)

                    # Store the t1 and t2 for this tup with the rest for this label.
                    uniq_labels_for_t1t2[best_label_for_this_tup].append(temp_T1andT2)

                    # Store the paq/byte rel mean for this tup with the rest for this label
                    uniq_labels_for_rel[best_label_for_this_tup].append(rel_mean)
                    
                except:
                    # first time
                    uniq_labels[best_label_for_this_tup] = state

                    # Make a tuple with the t1 and t2 to store for this tup
                    temp_T1andT2 = (model.T1_to_store, model.T2_to_store)
                    # Store the t1 and t2 for this tup with the rest for this label.
                    uniq_labels_for_t1t2[best_label_for_this_tup] = []
                    uniq_labels_for_t1t2[best_label_for_this_tup].append(temp_T1andT2)
                    
                    # First time for the rel too
                    uniq_labels_for_rel[best_label_for_this_tup] = []
                    uniq_labels_for_rel[best_label_for_this_tup].append(rel_mean)

                    # First time for the longest state too
                    uniq_labels_for_state[best_label_for_this_tup] = state

                    if debug > 3:
                        print '\tFirst State: {}'.format(state)

            # It can happend that there are only background labels...
            if uniq_labels == {}:
                print 'There are only Background labels?'
                sys.exit(-1)

            # 1- For each label
            for label in uniq_labels:
                if debug > 1:
                    print 'Creating MC for label: {}'.format(label)


                # 2- Conver the state to tuple
                t_states = tuple(list(uniq_labels[label]))
                # 3- Create a chain from that tuple
                p, P = pykov.maximum_likelihood_probabilities(t_states,lag_time=1, separator='#')

                median_rel = np.median(uniq_labels_for_rel[label])

                #prob_longest_state = np.float32(math.exp(P.walk_probability(uniq_labels_for_state[label])))
                prob_longest_state = P.walk_probability(uniq_labels_for_state[label])

                if debug > 2:
                    print '\tProbability vector: {}'.format(p)
                    print '\tTransition Matrix: {}'.format(P)
                    print '\tState: {}'.format(uniq_labels[label].strip('state='))
                    print '\tT1 and T2 values: {}'.format(uniq_labels_for_t1t2[label])
                    print '\tPaq/Bytes Rel: {}'.format(median_rel)
                    print '\tLongest state: {}. Prob: {:.30f}'.format(uniq_labels_for_state[label], prob_longest_state)
        
                # 4- store the chains in a file
                try:
                    os.mkdir(folder)
                except:
                    #dir exists
                    pass

                final_file = folder+'/'+label+'.mcmodel'
                output = open(final_file, 'wb')
                cPickle.dump(p,output)
                cPickle.dump(P,output)
                cPickle.dump(uniq_labels[label].strip('state='),output)
                cPickle.dump(uniq_labels_for_t1t2[label],output)
                cPickle.dump(median_rel,output)
                cPickle.dump(prob_longest_state,output)
                output.close()

            # Before storing the histogram, compute it
            self.histogram.compute_histograms()

            # Now store the histograms in the folder
            hist_file = folder+'/labels.histograms'
            output = open(hist_file, 'wb')
            cPickle.dump(self.histogram.data,output)
            #cPickle.dump(self.histogram.stb, output)
            #cPickle.dump(self.histogram.ttb, output)
            #cPickle.dump(self.histogram.ftb, output)
            #cPickle.dump(self.histogram.fdb, output)
            #cPickle.dump(self.histogram.sdb, output)
            #cPickle.dump(self.histogram.tdb, output)
            #cPickle.dump(self.histogram.fsb, output)
            #cPickle.dump(self.histogram.ssb, output)
            #cPickle.dump(self.histogram.tsb, output)
            #cPickle.dump(self.histogram.rb, output)

            output.close()

        except Exception as inst:
            print 'Problem in create_markov_chains()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            sys.exit(-1)


    def process_netflows(self,netflowFiles):
        """
        This function takes the netflowFile (or files) and parse it. 
        """
        try:
            global debug


            # Is the netflowFile a file or a group of files?
            files = netflowFiles.split(',')

            # Should survive between files
            self.netflow_id = 0

            # Print the csv headers if we need them
            if 'csv' in self.print_mode and self.tuple_to_analyze:
                #self.current_timestamp, self.id, self.tuple, self.best_label_for_this_tuple, self.bytes, self.srcdstbytesratio, self.duration, self.T2
                print 'timestamp,id,tuple,label,size,srcdstbytesratio,duration,timediff'
            elif 'csv' in self.print_mode and not self.tuple_to_analyze:
                print 'timestamp,id,tuple,label,bytesMedian,bytesMin,bytesMax,srcdstbytesratio,durationMedian,durationMin,durationMax,timediff,TDMedian,TDMin,TDMax'


            for netflowFile in files:
                if debug > 1:
                    print netflowFile
                if debug and 'csv' not in self.print_mode:
                    print 'Processing the netflow file {0}'.format(netflowFile)


                # Read the netflow and parse the input
                try:
                    # From stdin or file?
                    if netflowFile == '-':
                        f = sys.stdin
                        # Sometimes the netflows can take some time to arrive in the stdin..., if you have issues you can put a delay here.
                    else:
                        f = open(netflowFile,'r')
                except Exception as inst:
                    print 'Some problem opening the input netflow file. In process_netflow()'
                    print type(inst)     # the exception instance
                    print inst.args      # arguments stored in .args
                    print inst           # __str__ allows args to printed directly
                    sys.exit(-1)


                # The First line should be the header
                line = f.readline().strip()
                self.netflow_id += 1

                ##################
                # Argus processing...

                # Is the first line the labels?
                if not 'Start' in line:
                    print 'Warning! It seems that you miss the headers line. Please provide it as the first line in the archive.'
                    sys.exit(-1)

                columnDict = {}
                templateColumnArray = []
                columnArray = []
                columnNames = line.split(',')
                # We can try to guess the format by counting the ','  and ' '.

                #if debug:
                #    print 'Columns names: {0}'.format(columnNames)

                # So far argus does no have a column Date
                for col in columnNames:
                    columnDict[col] = ""
                    templateColumnArray.append(columnDict)
                    columnDict = {}

                columnArray = templateColumnArray

                # Read the second line to start processing
                line = f.readline().strip()
                self.netflow_id += 1

                # To store the netflows we should put the data in a dict
                self.original_netflows[self.netflow_id] = line


                while (line):
                    if debug > 10:
                        print 'Netflow line: {}\n'.format(line),

                    # Parse the columns
                    columnValues = line.split(',')

                    #if debug:
                    #    print columnValues

                    i = 0
                    for col in columnValues:
                        tempDict = columnArray[i]
                        tempDictName = tempDict.keys()[0]
                        tempDict[tempDictName] = col
                        columnArray[i] = tempDict
                        i += 1

                    #if debug:
                        #print columnArray

                    # Add the netflow to the model.
                    # The self.netflow_id is the id of this netflow
                    self.add_netflow(columnArray)

                    
                    # Go back to the empty array
                    columnArray = templateColumnArray

                    line = f.readline().strip()
                    self.netflow_id += 1
                    self.original_netflows[self.netflow_id] = line

                # End while

                if not 'csv' in self.print_mode and debug:
                    # Is -1 because when we try to read the last line, if there is no more lines we still add 1
                    print 'Amount of lines read: {0}'.format(self.netflow_id - 1)

        except Exception as inst:
            print 'Problem in process_netflow()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            sys.exit(-1)





def main():
    try:
        global debug
        global without_colors

        netflowFile = ""

        # Default Thresholds
        #olds with the difference
        #time_threshold_1 = timedelta(seconds=1.4)
        #time_threshold_2 = timedelta(seconds=3.4)
        #bytes_threshold_1 = float(6092)
        #bytes_threshold_2 = float(25841)
        #duration_threshold_1 = timedelta(seconds=3.3)
        #duration_threshold_2 = timedelta(seconds=7.6)
        #timeout_threshold = timedelta(seconds=3600)

        # new with the division
        # first try at new thresholds
        #time_threshold_1 = timedelta(seconds=0.0001)
        #time_threshold_2 = timedelta(seconds=0.1)
        #time_threshold_3 = timedelta(seconds=0.999)
        # second try at new thresholds
        time_threshold_1 = timedelta(seconds=1.05) # 1.7 before
        time_threshold_2 = timedelta(seconds=1.1)
        time_threshold_3 = timedelta(seconds=5)
        bytes_threshold_1 = float(1100)
        bytes_threshold_2 = float(5000)
        duration_threshold_1 = timedelta(seconds=0.1)
        duration_threshold_2 = timedelta(seconds=10)
        timeout_threshold = timedelta(seconds=3600)

        # Original thresholds
        """
        time_threshold_1 = timedelta(seconds=35)
        time_threshold_2 = timedelta(seconds=340)
        bytes_threshold_1 = float(125)
        bytes_threshold_2 = float(1100)
        duration_threshold_1 = timedelta(seconds=0.7)
        duration_threshold_2 = timedelta(seconds=2)
        timeout_threshold = timedelta(seconds=3600)
        """
        

        #min_probability_threshold = np.float32(1e-45)
        # 1e-45 ~ -100, 1e-5 ~ -11.5
        min_probability_threshold = 1
        max_probability_threshold = False
        label_to_analyze = ""
        tuple_to_analyze = ""
        # Default to 3
        tuple_mode = '3'
        print_mode = "oneline" 
        state_length = 3
        operation_mode = ''
        # By default only use CC models
        models_to_include = 'CC'
        number_of_folds = 10
        step_threshold = 0.1

        opts, args = getopt.getopt(sys.argv[1:], "aVD:hf:T:t:d:u:rvewl:p:L:P:q:n:s:R", ["all-models","help","version","debug=","file=","analyze-tuple=","time-threshold=","bytes-threshold=","duration-threshold=","tuple-mode=","training","validation","testing", "without-colors","state-length=","min-prob-threshold=","label=","print-mode=","max-prob-threshold=","num-folds=","step-threshold","thresholds"])
    except getopt.GetoptError: usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"): usage()
        if opt in ("-V", "--version"): version();sys.exit(-1)
        if opt in ("-D", "--debug"): debug = int(arg)
        if opt in ("-f", "--file"): netflowFile = str(arg)
        if opt in ("-t", "--time-threshold"): time_threshold_1 = timedelta(seconds=float(arg))
        if opt in ("-b", "--bytes-threshold"): bytes_threshold_1 = float(arg)
        if opt in ("-d", "--duration-threshold"): duration_threshold_2 = timedelta(seconds=float(arg))
        if opt in ("-T", "--analyze-tuple"): tuple_to_analyze = str(arg)
        if opt in ("-u", "--tuple-mode"): tuple_mode = str(arg)
        if opt in ("-R", "--thresholds"): operation_mode = 'Thresholds'
        if opt in ("-r", "--training"): operation_mode = 'Training'
        if opt in ("-v", "--validation"): operation_mode = 'Validation'
        if opt in ("-e", "--testing"): operation_mode = 'Testing'
        if opt in ("-w", "--without-colors"): without_colors = True
        if opt in ("-l", "--state-length"): state_length = int(arg)
        if opt in ("-p", "--min-prob-threshold"): min_probability_threshold = np.float32(arg)
        if opt in ("-q", "--max-prob-threshold"): max_probability_threshold = np.float32(arg)
        if opt in ("-L", "--label"): label_to_analyze = str(arg)
        if opt in ("-P", "--print-mode"): print_mode = str(arg)
        if opt in ("-a", "--all-models"): models_to_include = "all"
        if opt in ("-n", "--num-folds"): number_of_folds = int(arg)
        if opt in ("-s", "--step-threshold"): step_threshold = np.float32(arg)
    try:
        try:

            # If there is no max_probability_threshold specified, just use one prob, the minimun.
            if min_probability_threshold >= 0.0 and not max_probability_threshold:
                max_probability_threshold = min_probability_threshold
            elif min_probability_threshold and max_probability_threshold and (max_probability_threshold < min_probability_threshold):
                print 'The max probability threshold should be larger than min prob threshold.'
                sys.exit(1)

            if number_of_folds < 1:
                print 'The number of folds can not be less than 2'
                sys.exit(1)

            # Create an instance for the group of models
            stateModel = stateModels()
            stateModel.time_threshold_1 = time_threshold_1
            stateModel.time_threshold_2 = time_threshold_2
            stateModel.time_threshold_3 = time_threshold_3
            stateModel.bytes_threshold_1 = bytes_threshold_1
            stateModel.bytes_threshold_2 = bytes_threshold_2
            stateModel.duration_threshold_1 = duration_threshold_1
            stateModel.duration_threshold_2 = duration_threshold_2
            stateModel.timeout_threshold = timeout_threshold
            stateModel.min_probability_threshold = min_probability_threshold
            stateModel.max_probability_threshold = max_probability_threshold
            stateModel.tuple_mode = tuple_mode
            stateModel.label_to_analyze = label_to_analyze
            stateModel.tuple_to_analyze = tuple_to_analyze
            stateModel.print_mode = print_mode
            stateModel.state_length = state_length
            stateModel.models_to_include = models_to_include
            stateModel.number_of_folds = number_of_folds
            stateModel.step_threshold = step_threshold
            stateModel.operation_mode = operation_mode

            if netflowFile == "":
                usage()
                sys.exit(1)

            if state_length < 3 :
                print 'State length can not be less than 3'
                usage()
                sys.exit(1)

            elif print_mode and print_mode not in ["oneline","csv","epoch","normal","shortline"]:
                print 'You should use a valid print mode'
                usage()
                sys.exit(1)

            elif netflowFile != "":
                #if not tuple_to_analyze and not label_to_analyze:
                if not tuple_to_analyze and not label_to_analyze and netflowFile != "-" and not 'csv' in print_mode and not 'Threshold' in operation_mode:
                    version()
                elif 'Threshold' in operation_mode:
                    #print 'TD,T2,T1,bytes,duration,label'
                    print 'TD,bytes,duration,label'
                
                # Process of netflow flows
                stateModel.process_netflows(netflowFile)

                # Training
                if operation_mode == 'Training':
                    if debug:
                        print 'Training the markov chains model'
                        
                    # The markov chains should be created with the training data. In this case all the data is training
                    stateModel.tuples_dict_training = stateModel.tuples_dict

                    stateModel.create_markov_chains('./MCModels')

                elif operation_mode == 'Validation':
                    stateModel.train_and_validate(netflowFile)

                elif operation_mode == 'Testing':
                    if debug:
                        print 'Testing the markov chains model'

                    
                    while stateModel.min_probability_threshold <= (stateModel.max_probability_threshold) :
                        if debug:
                            print 'Testing with threshold:{} (max {})'.format(stateModel.min_probability_threshold, stateModel.max_probability_threshold)

                        stateModel.predict_labels_with_markov_chains('./MCModels')

                        stateModel.output_labeled_netflows(netflowFile + '.labeled')

                        # Compute the metrics
                        results = stateModel.compute_metrics(netflowFile + '.labeled')

                        #stateModel.min_probability_threshold /= stateModel.step_threshold
                        stateModel.min_probability_threshold += stateModel.step_threshold

                elif not stateModel.tuple_to_analyze and not 'Threshold' in operation_mode:
                    # Print the state of the model
                    stateModel.print_statistics()

            else:
                    usage()
                    sys.exit(1)

        except Exception, e:
                print "misc. exception (runtime error from user callback?):", e
        except KeyboardInterrupt:
                sys.exit(1)


    except KeyboardInterrupt:
        # CTRL-C pretty handling.
        print "Keyboard Interruption!. Exiting."
        sys.exit(1)


if __name__ == '__main__':
    main()

