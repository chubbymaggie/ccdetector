#! /usr/bin/env python
#  Copyright (C) 2009  Sebastian Garcia
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#
# Author:
# Sebastian Garcia, sebastian.garcia@agents.fel.cvut.cz, sgarcia@exa.unicen.edu.ar, eldraco@gmail.com
#
# Changelog

# Description
# A tool to manage the botnet models stored on disk. Still under development
#


# standard imports
import getopt
import sys
import re
import os
import numpy as np
import cPickle
import pykov
####################
# Global Variables

debug = 0
vernum = "0.1"
#########


# Print version information and exit
def version():
    print "+----------------------------------------------------------------------+"
    print "| ManageModel.py Version "+ vernum +"                                         |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print


# Print help information and exit:
def usage():
    print "+----------------------------------------------------------------------+"
    print "| ManageModel.py Version "+ vernum +"                                         |"
    print "| This program is free software; you can redistribute it and/or modify |"
    print "| it under the terms of the GNU General Public License as published by |"
    print "| the Free Software Foundation; either version 2 of the License, or    |"
    print "| (at your option) any later version.                                  |"
    print "|                                                                      |"
    print "| Author: Garcia Sebastian, eldraco@gmail.com                          |"
    print "| UNICEN-ISISTAN, Argentina. CTU, Prague-ATG                           |"
    print "+----------------------------------------------------------------------+"
    print "\nusage: %s <options>" % sys.argv[0]
    print "options:"
    print "  -h, --help                 Show this help message and exit"
    print "  -V, --version              Output version information and exit"
    print "  -D, --debug                Debug."
    print "  -o, --original-dir         Dir where the original models are stored."
    print "  -m, --merge-dir            Dir where the models to merge are stored."
    print
    sys.exit(1)


class models:
    """
    Holds and works with models.
    """
    def __init__(self, folder):
        self.folder = folder
        self.mcmodels = {}
        self.read_models()


    def read_models(self):
        """
        Read the models
        """
        try:
            global debug

            if debug > 0:
                print 'Folder used for the models: {}'.format(self.folder)

            # Read all the models
            list_of_files = os.listdir(self.folder)
            label_name = ""
            
            for file in list_of_files:
                try:
                    file_name = self.folder+'/'+file

                    # Ignore the background models, just in case some of them are there... Also ignore the histograms file
                    if 'Background' in file_name or 'histograms' in file_name:
                        continue

                    input = open(file_name, 'rb')
                    p = cPickle.load(input)
                    P = cPickle.load(input)
                    stored_state = cPickle.load(input)
                    t1_t2 = cPickle.load(input)
                    rel_median = cPickle.load(input)
                    p_original_state = cPickle.load(input)
                    input.close()
                    label_name = file.split('.mcmodel')[0]
                    if debug > 1:
                        print '\tFile: {}'.format(file)
                    if debug > 2:
                        print '\t\tp:{}, P:{}, stored_state:{}, t1_t2:{}, rel_median:{}, p_original_state:{}'.format(p,P,stored_state, t1_t2, rel_median, p_original_state)
                    
                    # Store the models. Here we don't care about t1 and t2 or rel_median
                    self.mcmodels[label_name] = [p,P,stored_state, t1_t2, rel_median,p_original_state]
                except:
                    print 'Error. The label {0} has no model stored.'.format(label_name)
            
            if not self.mcmodels:
                print 'Error. There is not models to read.'
                exit(-1)

        except Exception as inst:
            print 'Problem in read_models in models class()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            sys.exit(-1)


    def merge_models(self, merge_models):
        """
        Merge the models passed as argument with the models of this object.
        """
        try:
            global debug

            for merge_label in merge_models.mcmodels:
                if debug > 1:
                    print 'Label to merge: {}'.format(merge_label)

                for original_label in self.mcmodels:
                    if debug > 1:
                        print 'Original label: {}'.format(original_label)

                    # Find if we have this label
                    if merge_label == original_label:
                        if debug > 1:
                            print
                            print 'Merging label: {}'.format(original_label)

                        merge_p = merge_models.mcmodels[merge_label][0]
                        merge_P = merge_models.mcmodels[merge_label][1]
                        merge_states = merge_models.mcmodels[merge_label][2]
                        (merge_t1, merge_t2) = merge_models.mcmodels[merge_label][3][0]
                        merge_median_rel = merge_models.mcmodels[merge_label][4]
                        merge_prob_longest_state = merge_models.mcmodels[merge_label][5]
                        if debug > 2:
                            print '\tValues for merge. p={}, P={}, states={}, t1t2={}:{}, median_rel={}, prob_longest={:.20f}'.format(merge_p, merge_P, merge_states, merge_t1, merge_t2, merge_median_rel, merge_prob_longest_state)

                        original_p = self.mcmodels[merge_label][0]
                        original_P = self.mcmodels[merge_label][1]
                        original_states = self.mcmodels[merge_label][2]
                        (original_t1, original_t2) = self.mcmodels[merge_label][3][0]
                        original_median_rel = self.mcmodels[merge_label][4]
                        original_prob_longest_state = self.mcmodels[merge_label][5]
                        if debug > 2:
                            print '\tValues for original. p={}, P={}, states={}, t1t2={}:{}, median_rel={}, prob_longest={:.20f}'.format(original_p, original_P, original_states, original_t1, original_t2, original_median_rel, original_prob_longest_state)

                        # When you only have two values, median and mean are the same...
                        concatenated_states = original_states + '#' + merge_states
                        p, P = pykov.maximum_likelihood_probabilities(concatenated_states,lag_time=1, separator='#')
                        t1 = np.median([original_t1, merge_t1])
                        t2 = np.median([original_t2, merge_t2])
                        t1t2 = (t1,t2)
                        median_rel = np.median([original_median_rel, merge_median_rel])
                        prob_longest_state = np.median([original_prob_longest_state, merge_prob_longest_state])
                        if debug > 2:
                            print '\tMerged values. p={}, P={}, states={}, t1t2={}, median_rel={}, prob_longest={}'.format(p, P, concatenated_states, t1t2, median_rel, prob_longest_state)


                        print
                        print 'We do NOT store de values on disk!!'


                        # If you want to finish this... you should continue here.
                        # But what about the histograms???????????

                        # Store
                        #try:
                            #os.mkdir(folder)
                        #except:
                            ##dir exists
                            #pass

                        #final_file = folder+'/'+label+'.mcmodel'
                        #output = open(final_file, 'wb')
                        #cPickle.dump(p,output)
                        #cPickle.dump(P,output)
                        #cPickle.dump(uniq_labels[label].strip('state='),output)
                        #cPickle.dump(uniq_labels_for_t1t2[label],output)
                        #cPickle.dump(median_rel,output)
                        #cPickle.dump(prob_longest_state,output)
                        #output.close()

            
        except Exception as inst:
            print 'Problem in merge_models in models class()'
            print type(inst)     # the exception instance
            print inst.args      # arguments stored in .args
            print inst           # __str__ allows args to printed directly
            sys.exit(-1)



def main():
    try:
        global debug

        original_dir = ""
        merge_dir = ""

        opts, args = getopt.getopt(sys.argv[1:], "VD:ho:m:", ["help","version","debug=","original-dir=","merge-dir="])
    except getopt.GetoptError: usage()

    for opt, arg in opts:
        if opt in ("-h", "--help"): usage()
        if opt in ("-V", "--version"): version();sys.exit(-1)
        if opt in ("-D", "--debug"): debug = int(arg)
        if opt in ("-o", "--original-dir"): original_dir = str(arg)
        if opt in ("-m", "--merge-dir"): merge_dir = str(arg)
    try:
        try:
            if original_dir == "":
                usage()
                sys.exit(1)

            elif original_dir != "":
                version()

                    # 4.3 Compute all the values again
                    # 4.4 Replace the original file with the merge.

                # Create the original models object
                if debug:
                    print 'Orignal models'
                original_models = models(original_dir)

                if merge_dir != "":
                    if debug:
                        print '\nMerge models'
                    # Create the to merge models object
                    to_merge_models = models(merge_dir)

                    # Merge
                    original_models.merge_models(to_merge_models)
                

            else:
                    usage()
                    sys.exit(1)

        except Exception, e:
                print "misc. exception (runtime error from user callback?):", e
        except KeyboardInterrupt:
                sys.exit(1)


    except KeyboardInterrupt:
        # CTRL-C pretty handling.
        print "Keyboard Interruption!. Exiting."
        sys.exit(1)


if __name__ == '__main__':
    main()

